from __future__ import print_function

import numpy as np

from classification import IsLstmTsClassifier


def _load_data():
    """Load the data"""
    # path = '/home/haseeb/resources/data/kth_thesis/mix'
    # path = '/home/haseeb/resources/data/kth_thesis/corridor_5m_normal-traffic_no-pings/'
    path = '/home/haseeb/resources/data/kth_thesis/room_2m_0.1ms/rnn_all/'

    """
    4 classes
    """
    X_train = np.load(path + '/train_data_75_4.npy')
    y_train = np.load(path + '/train_labels_75_4.npy')
    X_test = np.load(path + '/test_data_75_4.npy')
    y_test = np.load(path + '/test_labels_75_4.npy')
    output_model_prefix = "wisture_all_classes"

    """
    2 classes : Swipe vs All
    """
    # X_train = np.load(path + '/train_data_75_4_swipe_vs_all_correct.npy')
    # y_train = np.load(path + '/train_labels_75_4_swipe_vs_all_correct.npy')
    # X_test = np.load(path + '/test_data_75_4_swipe_vs_all_correct.npy')
    # y_test = np.load(path + '/test_labels_75_4_swipe_vs_all_correct.npy')
    # output_model_prefix = "wisture_swipe_vs_all_correct"

    """
    2 classes : pull vs All
    """
    # X_train = np.load(path + '/train_data_75_4_pull_vs_all_correct.npy')
    # y_train = np.load(path + '/train_labels_75_4_pull_vs_all_correct.npy')
    # X_test = np.load(path + '/test_data_75_4_pull_vs_all_correct.npy')
    # y_test = np.load(path + '/test_labels_75_4_pull_vs_all_correct.npy')
    # output_model_prefix = "wisture_pull_vs_all_correct"

    """
    2 classes : push vs All
    """
    # X_train = np.load(path + '/train_data_75_4_push_vs_all_correct.npy')
    # y_train = np.load(path + '/train_labels_75_4_push_vs_all_correct.npy')
    # X_test = np.load(path + '/test_data_75_4_push_vs_all_correct.npy')
    # y_test = np.load(path + '/test_labels_75_4_push_vs_all_correct.npy')
    # output_model_prefix = "wisture_push_vs_all_correct"

    return X_train, y_train, X_test, y_test, output_model_prefix


if __name__ == '__main__':
    # load the data
    X_train, y_train, X_test, y_test, output_model_prefix = _load_data()
    # fit a model
    classifier = IsLstmTsClassifier(drop_prob=0.5, num_layers=1, hidden_size=100, num_steps=7, max_iterations=1000,
                                    batch_size=10,
                                    eta=0.01, init_scale=0.08, max_grad_norm=25,
                                    model_name_prefix=output_model_prefix)
    classifier.fit(X_train, y_train)
    print("Total accuracy : " + str(classifier.score(X_test, y_test)))
    # accuracy per class
    classes = np.unique(y_test)
    for class_label in classes:
        class_index = y_test == class_label
        class_X = X_test[class_index[:, 0], :]
        class_y = y_test[class_index[:, 0], :]
        print("Class " + str(class_label) + " accuracy : " + str(classifier.score(class_X, class_y)))

from __future__ import print_function

"""LSTM for time series classification
Made: 30 march 2016

This model takes in time series and class labels.
The LSTM models the time series. A fully-connected layer
generates an output to be classified with Softmax
"""
import numpy as np
from sklearn import grid_search

from classification import IsLstmTsClassifier


def _load_data():
    """Load the data"""
    # path = '/home/haseeb/resources/data/kth_thesis/mix'
    path = '/home/haseeb/resources/data/kth_thesis/corridor_5m_normal-traffic_no-pings/'
    X_train = np.load(path + '/train_data_75_4.npy')
    y_train = np.load(path + '/train_labels_75_4.npy')
    X_test = np.load(path + '/test_data_75_4.npy')
    y_test = np.load(path + '/test_labels_75_4.npy')
    return X_train, y_train, X_test, y_test


if __name__ == '__main__':
    # load the data
    X_train, y_train, X_test, y_test = _load_data()
    # fit a model
    classifier = IsLstmTsClassifier()
    params = {'init_scale': [0.08, 0.04],  # Initial scale for the states
              'max_grad_norm': [25],  # Clipping of the gradient before update
              'num_steps': [4],  # Number of steps to backprop over at every batch
              'step_size': [5],
              'drop_prob': [0.5],
              'num_layers': [1, 2],  # Number of stacked LSTM layers
              'hidden_size': [50, 100, 200],  # Number of entries of the cell state of the LSTM
              'max_iterations': [10000, 20000],  # Maximum iterations to train
              'batch_size': [10],  # Batch size,
              'eta':[1e-1],
              'X_val': [X_test],
              'y_val': [y_test]
              }
    """
    window size?
    smoothing factor?
    """
    classifier_cv = grid_search.GridSearchCV(classifier, params,
                                             scoring='accuracy',
                                             cv=4,
                                             n_jobs=1,
                                             error_score=0)
    classifier_cv.fit(X_train, y_train)
    print("Best params")
    print(classifier_cv.best_params_)
    print("Best Score")
    print(classifier_cv.best_score_)
    print("Best Estimator score")
    best_estimator = classifier_cv.best_estimator_
    print(best_estimator.score(X_test, y_test))
    print("Classifer_cv.score()")
    print(classifier_cv.score(X_test, y_test))

import numpy as np
import tensorflow as tf
from tensorflow.core.framework import graph_pb2

graph_apth = "models/wisture_all_classes_full"


def load_graph(path):
    sess = tf.Session()
    with open(path, 'rb') as f:
        graph_def = graph_pb2.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
    prediction = sess.graph.get_tensor_by_name('Softmax/prediction:0')
    print(prediction)
    x, y = get_data()
    X = np.expand_dims(x, axis=3)

    # print(tf.get_collection())
    predictions = np.zeros(X.shape[0])
    for i in range(X.shape[0]):
        predictions[i] = sess.run(prediction, feed_dict={
            'input_data:0': X[i:i + 1, :]
        })
    print("Prediction: {}".format(predictions))
    print("Truth:      {}".format(y))


def get_data():
    path = '/home/haseeb/resources/data/kth_thesis/room_2m_0.1ms/rnn_all/'
    return np.load(path + '/test_data_75_4.npy'), np.load(path + '/test_labels_75_4.npy')[:, 0]


if __name__ == '__main__':
    load_graph(graph_apth)
    print("done")

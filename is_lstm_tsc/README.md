# Build in python, test in C++
## Prerequisites
- Tensorflow built from source

### Build the model in python
### Export the trained graph
### Create the C++ code that laods the trained graph
### Compile with bazel
- cd to the source file dir
  /nest/garage/tf/python_cpp/tensorflow/tensorflow/my_projs/wisture
- create the bazedl build file
- compile 
  bazel build :<target that builds ur c++ file>
- Run the compiled code
  /nest/garage/tf/python_cpp/tensorflow/bazel-bin/tensorflow/my_projs/wisture

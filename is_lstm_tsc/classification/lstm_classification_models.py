from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.base import BaseEstimator

from .freeze_graph import freeze_graph


class IsLstmTsClassifier(BaseEstimator):
    def __init__(self, init_scale=0.08, max_grad_norm=25, num_steps=4, step_size=5, drop_prob=0.5,
                 num_layers=2, hidden_size=70, max_iterations=18000, batch_size=20, eta=8e-3, models_dir="models/",
                 model_name_prefix="trained_model"):
        self.logits = None
        tf.reset_default_graph()
        self.output_classes = None
        self.y_val = None
        self.X_val = None
        self.keep_prob = None
        self.targets = None
        self.input_data = None
        self.test_accuracy = None
        self.predictions = None
        self.session = None
        self.models_dir = models_dir
        self.model_name_prefix = model_name_prefix
        """Hyperparamaters"""
        self.init_scale = init_scale  # Initial scale for the states
        self.max_grad_norm = max_grad_norm  # Clipping of the gradient before update
        self.num_steps = num_steps  # Number of steps to backprop over at every batch
        self.step_size = step_size
        self.drop_prob = drop_prob
        self.num_layers = num_layers  # Number of stacked LSTM layers
        self.hidden_size = hidden_size  # Number of entries of the cell state of the LSTM
        self.max_iterations = max_iterations  # Maximum iterations to train
        self.batch_size = batch_size  # Batch size
        self.eta = eta

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
        return self

    def fit(self, X, y, x_val=None, y_val=None, save_graph=False, plot_performance=False):
        X = np.expand_dims(X, axis=3)
        if x_val is None:
            print("using the train data as validation set")
            self.X_val = X
            self.y_val = y
        else:
            self.X_val = np.expand_dims(x_val, axis=3)
            self.y_val = y_val
        # figure out the number of classes
        self.output_classes = np.unique(y).shape[0]
        """Place holders"""
        self.input_data = tf.placeholder(tf.float32, [None, self.num_steps, self.step_size, 1], name='input_data')
        self.targets = tf.placeholder(tf.int64, [None], name='Targets')

        # Used later on for drop_out. At testtime, we pass 1.0
        self.keep_prob = tf.placeholder("float", name='Drop_out_keep_prob')

        initializer = tf.random_uniform_initializer(-self.init_scale, self.init_scale)
        with tf.variable_scope("model", initializer=initializer):

            """Define the basis LSTM"""
            with tf.name_scope("LSTM_setup"):
                lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(self.hidden_size)
                lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=self.keep_prob)
                cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell] * self.num_layers)

            """Define the recurrent nature of the LSTM"""
            inputs = [tf.squeeze(input_step, [1, 3]) for input_step in tf.split(1, self.num_steps, self.input_data)]
            outputs, _ = tf.nn.rnn(cell, inputs, dtype=tf.float32)
        output = outputs[-1]

        """Generate a classification from the last cell_output"""
        # Note, this is where timeseries classification differs from sequence to sequence
        # modelling. We only output to Softmax at last time step
        with tf.name_scope("Softmax"):
            with tf.variable_scope("Softmax_params", initializer=initializer):
                softmax_w = tf.get_variable("softmax_w", [self.hidden_size, self.output_classes])
                softmax_b = tf.get_variable("softmax_b", [self.output_classes])
            logits = tf.add(tf.matmul(output, softmax_w), softmax_b, name='logits')
            self.logits = logits
            softmax = tf.nn.softmax(logits, name="softmax")
            # Use sparse Softmax because we have mutually exclusive classes
            loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits, self.targets, name='Sparse_softmax')
            cost = tf.reduce_sum(loss) / self.batch_size
            # Calculate the accuracy
            self.predictions = tf.argmax(logits, 1)
            predict = tf.to_int32(self.predictions, name="prediction")
            correct_prediction = tf.equal(predict, tf.to_int32(self.targets))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
            accuracy_summary = tf.scalar_summary("accuracy", accuracy)
            with tf.device("/cpu:0"):
                self.test_accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

        """Optimizer"""
        with tf.name_scope("Optimizer"):
            tvars = tf.trainable_variables()
            # We clip the gradients to prevent explosion
            grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars), self.max_grad_norm)
            optimizer = tf.train.AdamOptimizer(learning_rate=self.eta)
            gradients = zip(grads, tvars)
            train_op = optimizer.apply_gradients(gradients)

        # Collect the costs in a numpy fashion
        epochs = np.floor(self.batch_size * self.max_iterations / X.shape[0])
        print('Train with approximately %d epochs' % (epochs))
        perf_collect = np.zeros((3, int(np.floor(self.max_iterations / 100))))

        # Add ops to save and restore all the variables.
        saver = tf.train.Saver()

        """Training"""
        self.session = tf.Session()
        # initialize all variables
        tf.initialize_all_variables().run(session=self.session)

        step = 0
        for i in range(self.max_iterations):

            # Sample batch for training
            X_batch, y_batch = self._sample_batch(X, y, self.batch_size)

            # Next line does the actual training
            self.session.run(train_op,
                             feed_dict={self.input_data: X_batch,
                                        self.targets: y_batch
                                        ,self.keep_prob: self.drop_prob
                                        }
                             )

            if i % 100 == 0:
                # Evaluate training performance
                X_batch, y_batch = self._sample_batch(X, y, self.batch_size)
                train_cost, train_acc = self.session.run([cost, accuracy],
                                                         feed_dict={self.input_data: X_batch,
                                                                    self.targets: y_batch
                                                                    , self.keep_prob: 1
                                                                    }
                                                         )

                # Evaluate validation performance
                X_batch, y_batch = self._sample_batch(self.X_val, self.y_val, self.batch_size)
                val_acc = self.session.run(accuracy,
                                           feed_dict={self.input_data: X_batch,
                                                      self.targets: y_batch
                                                      , self.keep_prob: 1
                                                      }
                                           )

                perf_collect[0, step] = train_cost
                perf_collect[1, step] = train_acc
                perf_collect[2, step] = val_acc


                print(
                    'At %d out of %d train acc is %.3f and val acc is %.3f' % (
                        i, self.max_iterations, train_acc, val_acc))

                step += 1

        if save_graph:
            """Saving the model graph"""
            modelFileName = self.model_name_prefix + ".pb"
            tf.train.write_graph(self.session.graph_def, self.models_dir, modelFileName, as_text=False)

            """ Saving the model variables"""
            modelVariablesFileName = os.path.join(self.models_dir, "model_variables_" + str(self.max_iterations))
            saver.save(self.session, modelVariablesFileName)

            """ Creating a single graph with the model variables converted to constants with values equal to the trained
            model"""
            model_file_path = os.path.join(self.models_dir, modelFileName)
            output_graph_path = os.path.join(self.models_dir, self.model_name_prefix + "_full")

            freeze_graph(input_graph=model_file_path,
                         input_saver="",
                         input_binary=True,
                         input_checkpoint=modelVariablesFileName,
                         output_node_names="Softmax/logits,Softmax/prediction,Softmax/softmax",
                         restore_op_name="save/restore_all",
                         filename_tensor_name="save/Const:0",
                         output_graph=output_graph_path,
                         clear_devices=True,
                         initializer_nodes="")

        if plot_performance:
            plt.figure()
            plt.plot(perf_collect.T)
            plt.legend(['train_cost', 'train_acc', 'val_acc'], loc=4)
            plt.show()

    def score(self, X, y):
        y_prediction = self.predict(X)
        print("     Truth : " + str(y[:, 0]))
        print("Prediction : " + str(y_prediction[:]))
        return np.mean(np.equal(y_prediction, y[:, 0]))

    def predict(self, X):
        X = np.expand_dims(X, axis=3)
        predictions = np.zeros(X.shape[0])
        for i in range(X.shape[0]):
            predictions[i] = self.session.run(self.predictions,
                                              feed_dict={
                                                  self.input_data: X[i:i + 1, :]
                                                  , self.keep_prob: 1
                                              })
        return predictions

    def get_logits(self, X):
        X = np.expand_dims(X, axis=3)
        logits = np.zeros((X.shape[0], self.output_classes))
        for i in range(X.shape[0]):
            logits[i, :] = self.session.run(self.logits,
                                            feed_dict={
                                                self.input_data: X[i:i + 1, :]
                                                , self.keep_prob: 1
                                            })
        return logits

    def _sample_batch(self, X_train, y_train, batch_size):
        """ Function to sample a batch for training"""
        N = X_train.shape[0]
        ind_N = np.random.choice(N, batch_size, replace=False)
        # form batch
        X_batch = X_train[ind_N, :]
        y_batch = y_train[ind_N, 0]
        return X_batch, y_batch

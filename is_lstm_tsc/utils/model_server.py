import numpy as np
import tensorflow as tf
from tensorflow.core.framework import graph_pb2


class ModelServer:
    def __init__(self, model_path, input_tensor_name, output_tensor_name):
        self.session = tf.Session()
        with open(model_path, 'rb') as f:
            graph_def = graph_pb2.GraphDef()
            graph_def.ParseFromString(f.read())
            self.session.graph.as_default()
            tf.import_graph_def(graph_def, name='')
            self.output = self.session.graph.get_tensor_by_name(output_tensor_name + ':0')
            self.input_tensor_name = input_tensor_name

    def run(self, input_data):
        return self.session.run(self.output,
                                feed_dict={
                                    self.input_tensor_name + ':0': input_data
                                })


def get_data():
    path = '/home/haseeb/resources/data/kth_thesis/room_2m_0.1ms/rnn_all/'
    return np.load(path + '/test_data_75_4.npy'), np.load(path + '/test_labels_75_4.npy')[:, 0]


if __name__ == '__main__':
    server = ModelServer(model_path="models/wisture_all_classes_full",
                         input_tensor_name='input_data',
                         output_tensor_name='Softmax/prediction')

    x, y = get_data()
    X = np.expand_dims(x, axis=3)
    predictions = np.zeros(X.shape[0])
    for i in range(X.shape[0]):
        predictions[i] = server.run(input_data=X[i:i + 1, :])
    print("Prediction: {}".format(predictions))
    print("Truth:      {}".format(y))
